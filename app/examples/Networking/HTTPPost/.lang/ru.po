# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
# Translators:
# Дмитрий Ошкало <dmitry.oshkalo@gmail.com>, 2019
# Kашицин Роман <calorus@gmail.com>, 2019
# Олег o1hk <o1h2k3@yandex.ru>, 2019
# AlexL <loginov.alex.valer@gmail.com>, 2019
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-23 07:08+0300\n"
"PO-Revision-Date: 2019-05-09 00:48+0000\n"
"Last-Translator: AlexL <loginov.alex.valer@gmail.com>, 2019\n"
"Language-Team: Russian (https://www.transifex.com/rus-open-source/teams/44267/ru/)\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#: app/examples/Networking/HTTPPost/.project:21
msgid "HTTP client POST example"
msgstr "Пример POST HTTP-клиента"

#: app/examples/Networking/HTTPPost/.project:22
msgid ""
"HttpClient POST request example.\n"
"\n"
"This example how to send a POST HTTP request to a server and get the response. You can configure an HTTP proxy if needed."
msgstr ""
"Пример POST-запроса клиента HTTP.\n"
"\n"
"В этом примере показано, как отправить запрос POST HTTP на сервер и получить ответ. При необходимости вы можете настроить HTTP-прокси."

#: app/examples/Networking/HTTPPost/.src/FHttpPost.class:7
msgid "Error "
msgstr "Ошибка "

#: app/examples/Networking/HTTPPost/.src/FHttpPost.class:22
msgid "Waiting for reply..."
msgstr "Ожидание ответа..."

#: app/examples/Networking/HTTPPost/.src/FHttpPost.class:33
msgid "OK"
msgstr "ОК"

#: app/examples/Networking/HTTPPost/.src/FHttpPost.class:59 app/examples/Networking/HTTPPost/.src/FHttpPost.class:62
msgid "Error"
msgstr "Ошибка"

#: app/examples/Networking/HTTPPost/.src/FHttpPost.class:72
msgid "Connecting..."
msgstr "Соединение..."

#: app/examples/Networking/HTTPPost/.src/FHttpPost.form:5
msgid "Internet Calculator"
msgstr "Интернет-калькулятор"

#: app/examples/Networking/HTTPPost/.src/FHttpPost.form:9
msgid "Post"
msgstr "Отправить"

#: app/examples/Networking/HTTPPost/.src/FHttpPost.form:20
msgid "HTTP Headers"
msgstr "Заголовки HTTP"

#: app/examples/Networking/HTTPPost/.src/FHttpPost.form:25
msgid "HTTP Data"
msgstr "Данные HTTP"

#: app/examples/Networking/HTTPPost/.src/FHttpPost.form:29
msgid "Write Here a Number :"
msgstr "Написать здесь число:"

#: app/examples/Networking/HTTPPost/.src/FHttpPost.form:33
msgid "Write Here Another Number :"
msgstr "Написать здесь другое число:"

#: app/examples/Networking/HTTPPost/.src/FHttpPost.form:37 app/examples/Networking/HTTPPost/.src/FHttpPost.form:41
msgid "5"
msgstr "5"

#: app/examples/Networking/HTTPPost/.src/FHttpPost.form:45
msgid "Finally press the Post button >>>"
msgstr "В заключение нажать кнопку >>>"

#: app/examples/Networking/HTTPPost/.src/FHttpPost.form:52
msgid "Using the power of internet to have a minimal calculator machine!"
msgstr "Использование возможностей Интернета, чтобы иметь минимальный калькулятор!"

#: app/examples/Networking/HTTPPost/.src/FHttpPost.form:78
msgid "Use Proxy"
msgstr "Использовать прокси"

#: app/examples/Networking/HTTPPost/.src/FHttpPost.form:83
msgid "127.0.0.1:3128"
msgstr "127.0.0.1:3128"

