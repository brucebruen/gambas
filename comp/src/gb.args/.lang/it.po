#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.args 3.12.90\n"
"PO-Revision-Date: 2019-01-15 14:22 UTC\n"
"Last-Translator: gian <gian@gian>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "gb.args"
msgstr "-"

#: Args.module:73
msgid "Display version"
msgstr "Mostra la versione"

#: Args.module:78
msgid "Display this help"
msgstr "Mostra questo aiuto"

#: Args.module:82
msgid "Usage: &1 <options> <arguments>"
msgstr "Utilizza: &1 <options> <arguments>"

#: Args.module:85
msgid "Options:"
msgstr "Opzioni:"

#: Args.module:92
msgid "unknown option: &1"
msgstr "opzione sconosciuta: &1"

